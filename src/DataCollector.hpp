/**
 * File:   DataCollector.hpp
 * Author: tfischer
 * 
 * iData, an interactive data plotting software tool, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef IDATA_DATA_COLLECTOR_HPP_
#define IDATA_DATA_COLLECTOR_HPP_

/**
 * Abstract interface for a data Collector
 */
class DataCollector
{
	public:

		/**
		 * Function that should signal whenever a new sample arrives
		 */
		virtual sigc::signal<void, uint32_t, const double*>& OnNewData() = 0;
};

#endif // IDATA_DATA_COLLECTOR_HPP_
