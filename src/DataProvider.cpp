#include "DataProvider.hpp"

DataProvider::DataProvider(double range_min, double range_max)
	: range_min_(range_min), range_max_(range_max)
{}

DataProvider::~DataProvider()
{}

DataProvider::Sample::Sample(double timestamp, double value)
	: timestamp_(timestamp), value_(value)
{}

double DataProvider::Sample::getTime() const
{
	return timestamp_;
}

double DataProvider::Sample::getValue() const
{
	return value_;
}

double DataProvider::getRangeMin() const
{
	return range_min_;
}

double DataProvider::getRangeMax() const
{
	return range_max_;
}

DataProvider::const_reverse_iterator DataProvider::rbegin() const
{
	return data_.rbegin();
}

DataProvider::const_reverse_iterator DataProvider::rend() const
{
	return data_.rend();
}
