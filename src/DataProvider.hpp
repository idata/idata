/**
 * File:   DataProvider.hpp
 * Author: tfischer
 * 
 * iData, an interactive data plotting software tool, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef IDATA_DATA_PROVIDER_HPP_
#define IDATA_DATA_PROVIDER_HPP_

#include <deque>

class DataProvider
{
	public:

		DataProvider(double range_min, double range_max);

		virtual ~DataProvider();

		/**
		 * Sample container class
		 */
		class Sample
		{
			public:

				Sample(double timestamp, double value);

				double getTime() const;

				double getValue() const;
				
			private:

				double timestamp_, value_;
		};

		double getRangeMin() const;

		double getRangeMax() const;

		typedef std::deque<Sample>::const_reverse_iterator const_reverse_iterator;

		const_reverse_iterator rbegin() const;

		const_reverse_iterator rend() const;

	protected:

		std::deque<Sample> data_;

		double range_min_, range_max_;
};

#endif // IDATA_DATA_PROVIDER_HPP_
