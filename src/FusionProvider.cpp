#include "FusionProvider.hpp"

DummyDataProvider::DummyDataProvider(double range_min, double range_max)
	: DataProvider(range_min, range_max)
{}

void DummyDataProvider::update(double timestamp, double value)
{
	DataProvider::Sample new_sample(timestamp, value);
	data_.push_back(new_sample);
}

// ================================================================== //

DummyVectorProvider::DummyVectorProvider(double range_min, double range_max)
	: data_x_(range_min, range_max), data_y_(range_min, range_max), data_z_(range_min, range_max)
{}

void DummyVectorProvider::update(double timestamp, const Eigen::Vector3d& data)
{
	data_x_.update(timestamp, data[0]);
	data_y_.update(timestamp, data[1]);
	data_z_.update(timestamp, data[2]);
}

const DataProvider& DummyVectorProvider::getDataX() const
{
	return data_x_;
}
const DataProvider& DummyVectorProvider::getDataY() const
{
	return data_y_;
}
const DataProvider& DummyVectorProvider::getDataZ() const
{
	return data_z_;
}

// ================================================================== //
/*
FusionProvider::FusionProvider(GY80DataCollector& collector)
	: data_x_(-1.1, 1.1), data_y_(-1.1, 1.1), data_z_(-1.1, 1.1)
{
	// TODO ver si puedo hacer un signal a la función virtual directamente
	data_connection_ = collector.onNewSample().connect( sigc::mem_fun(*this, &GravityProvider::handleNewSample_) );
}

FusionProvider::~FusionProvider()
{}

const DataProvider& FusionProvider::getDataX() const
{
	return data_x_;
}

const DataProvider& FusionProvider::getDataY() const
{
	return data_y_;
}

const DataProvider& FusionProvider::getDataZ() const
{
	return data_z_;
}

void FusionProvider::handleNewSample_(const GY80Sample& sample)
{
	handleUpdate(sample);
}

// ================================================================== //

GravityProvider::GravityProvider(GY80DataCollector& collector, const Eigen::Vector3d& acc_ini)
	: FusionProvider(collector), gravity_(acc_ini)
{}

void GravityProvider::handleUpdate(const GY80Sample& sample)
{
	// compute new gravity value
	Eigen::Vector3d g = gravity_.update(sample.acc);

	data_x_.update(sample.timestamp, g[0]);
	data_y_.update(sample.timestamp, g[1]);
	data_z_.update(sample.timestamp, g[2]);
}

// ================================================================== //

LinearAccelerationProvider::LinearAccelerationProvider(GY80DataCollector& collector, const GravityFuser& gravity)
	: FusionProvider(collector), lin_acc_(gravity)
{}

void LinearAccelerationProvider::handleUpdate(const GY80Sample& sample)
{
	// compute new gravity value
	Eigen::Vector3d g = lin_acc_.update(sample.acc);

	data_x_.update(sample.timestamp, g[0]);
	data_y_.update(sample.timestamp, g[1]);
	data_z_.update(sample.timestamp, g[2]);
}
*/
// ================================================================== //

GY80Fuser::GY80Fuser(const GY80Sample& sample_ini)
	//: gravity_from_raw_acc_(sample_ini.acc)
	: gravity_( sample_ini.timestamp, sample_ini.acc )
	//, gravity_from_comp_
	, mag_(sample_ini.mag)
	// mag comp
	, north_(mag_, gravity_)
	, linear_acc_(gravity_)
	, att_from_pos_(north_, gravity_)
	//, att_from_gyro_()
	// att comp
	, attitude_( att_from_pos_ )
	// --- providers ---
	, gyro_provider_(-360, 360)
	, acc_provider_(-1.1, 1.1)
	, mag_provider_(-500, 500)
	, gravity_provider_(-2, 2)
	, north_provider_(-1.1, 1.1)
	, linear_acc_provider_(-2, 2)
{}

void GY80Fuser::handleUpdate(const GY80Sample& sample)
{
	//gravity_from_raw_acc_.update(sample.acc);
	gravity_.update(sample.timestamp, sample.gyro, sample.acc);
	//, gravity_from_comp_
	mag_.update(sample.mag);
	// mag comp
	north_.update();
	linear_acc_.update(sample.acc);
	att_from_pos_.update();
	//att_from_gyro_()
	// att comp

	gyro_provider_.update(sample.timestamp, sample.gyro);
	acc_provider_.update(sample.timestamp, sample.acc);
	mag_provider_.update(sample.timestamp, sample.mag);

	gravity_provider_.update(sample.timestamp, gravity_.value());
	north_provider_.update(sample.timestamp, north_.value());

	linear_acc_provider_.update(sample.timestamp, linear_acc_.value());
}

const VectorProvider& GY80Fuser::getGyro() const
{
	return gyro_provider_;
}

const VectorProvider& GY80Fuser::getAcc() const
{
	return acc_provider_;
}

const VectorProvider& GY80Fuser::getMag() const
{
	return mag_provider_;
}

const VectorProvider& GY80Fuser::getGravity() const
{
	return gravity_provider_;
}

const VectorProvider& GY80Fuser::getNorth() const
{
	return north_provider_;
}

const VectorProvider& GY80Fuser::getLinearAcceleration() const
{
	return linear_acc_provider_;
}
