#ifndef FUSION_PROVIDER_HPP_
#define FUSION_PROVIDER_HPP_

#include "DataProvider.hpp"
#include "GY80DataServer.hpp"
#include "VectorProvider.hpp"

// import from imu project
#include "gravity.hpp"
#include "attitude.hpp"

class DummyDataProvider : public DataProvider
{
	public:

		DummyDataProvider(double range_min, double range_max);

		void update(double timestamp, double value);
};

class DummyVectorProvider : public VectorProvider
{
	public:

		DummyVectorProvider(double range_min, double range_max);

		void update(double timestamp, const Eigen::Vector3d& data);

		const DataProvider& getDataX() const;
		const DataProvider& getDataY() const;
		const DataProvider& getDataZ() const;

	private:

		DummyDataProvider data_x_, data_y_, data_z_;
};

/*

class FusionProvider : public VectorProvider
{
	public:
	
		FusionProvider(GY80DataCollector& collector);
		~FusionProvider();
		
		const DataProvider& getDataX() const;
		const DataProvider& getDataY() const;
		const DataProvider& getDataZ() const;

	protected:

		DummyDataProvider data_x_, data_y_, data_z_;

		virtual void handleUpdate(const GY80Sample& sample) = 0;

	private:

		sigc::connection data_connection_;

		void handleNewSample_(const GY80Sample& sample);
};

class GravityProvider : public FusionProvider
{
	public:

		GravityProvider(GY80DataCollector& collector, const Eigen::Vector3d& acc_ini);

	protected:

		void handleUpdate(const GY80Sample& sample);

	private:

		GravityFromRawAcc gravity_;
};

class LinearAccelerationProvider : public FusionProvider
{
	public:

		LinearAccelerationProvider(GY80DataCollector& collector, const GravityFuser& gravity);

	protected:

		void handleUpdate(const GY80Sample& sample);

	private:

		LinearAcceleration lin_acc_;
};

*/

class GY80Fuser
{
	public:

		GY80Fuser(const GY80Sample& sample_ini);

		void handleUpdate(const GY80Sample& sample);

		const VectorProvider& getGyro() const;
		const VectorProvider& getAcc() const;
		const VectorProvider& getMag() const;

		const VectorProvider& getGravity() const;
		const VectorProvider& getNorth() const;
		const VectorProvider& getLinearAcceleration() const;

	private:

		//GravityFromRawAcc gravity_from_raw_acc_;
		//GravityFromComplementary gravity_from_comp_;
		GravityFromComplementary gravity_;

		MagneticFieldFromRawMag mag_;
		// TODO mag_from_comp_; (mag + gyro)

		MagneticNorthFuser north_;
		LinearAcceleration linear_acc_;

		AttitudeFromPositional att_from_pos_;
		//AttitudeFromRawGyro att_from_gyro_;
		// TODO att from comp (raw + pos)
		AttitudeFuser& attitude_;

		DummyVectorProvider gyro_provider_;
		DummyVectorProvider acc_provider_;
		DummyVectorProvider mag_provider_;

		DummyVectorProvider gravity_provider_;
		DummyVectorProvider north_provider_;
		DummyVectorProvider linear_acc_provider_;
};

#endif // FUSION_PROVIDER_HPP_
