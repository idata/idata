#include <string.h>			/* bzero */
#include <unistd.h>			/* read */
#include <assert.h>			/* assert */

#include <netinet/in.h>
#include "useful_macros.hpp"
#include "GY80DataServer.hpp"

void GY80DataCollector::connect(int portno)
{
	struct sockaddr_in serv_addr;

	/* First call to socket() function */
	sockfd_ = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd_ < 0) 
	{
		perror("ERROR opening socket");
		exit(1);
	}
	/* Initialize socket structure */
	bzero((char *) &serv_addr, sizeof(serv_addr));

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(portno);

	/* Now bind the host address using bind() call.*/
	if (bind(sockfd_, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
	{
		perror("ERROR on binding");
		exit(1);
	}

	/* Now start listening for the clients, here process will
	* go in sleep mode and will wait for the incoming connection
	*/
	listen(sockfd_, 5);
}

void GY80DataCollector::waitForClient()
{
	printf("wait for client... ");

	struct sockaddr_in cli_addr;
	unsigned int clilen = sizeof(cli_addr);
	/* Accept actual connection from the client */
	client_sockfd_ = accept(sockfd_, (struct sockaddr *)&cli_addr, &clilen);
	if (client_sockfd_ < 0) 
	{
		perror("ERROR on accept");
		exit(1);
	}
	
	printf("client accepted!\n");
}

void GY80DataCollector::initAsyncRead()
{
	// Let Gtk/Glib check for data avaibility and exceptions and call a handler
	connection_on_data_ready_ = Glib::signal_io().connect( sigc::mem_fun(*this, &GY80DataCollector::handleNewData_), client_sockfd_, Glib::IO_IN | Glib::IO_ERR );
}

bool GY80DataCollector::waitForSample(GY80Sample& sample)
{
	double buffer[GY80_NDATA+2];

	if (!waitForData_(buffer))
		return false;

	sample.timestamp = buffer[GY80_TIME_IDX] / 1000.0;
	sample.gyro = Eigen::Vector3d(buffer[0], buffer[1], buffer[2]);
	sample.acc = Eigen::Vector3d(buffer[3], buffer[4], buffer[5]);
	sample.mag = Eigen::Vector3d(buffer[6], buffer[7], buffer[8]);

	return true;
}

/*
sigc::signal<void, const double*>& GY80DataCollector::onNewData()
{
	return on_new_data_;
}
*/

sigc::signal<void, const GY80Sample&>& GY80DataCollector::onNewSample()
{
	return on_new_sample_;
}

sigc::signal<void>& GY80DataCollector::onNewClient()
{
	return on_new_client_;
}

sigc::signal<void>& GY80DataCollector::onClientLost()
{
	return on_client_lost_;
}

bool GY80DataCollector::waitForData_(double* buffer)
{
	int n = read( client_sockfd_, buffer, (GY80_NDATA+1)*sizeof(double)+1 );
	if (n < 0)
	{
		perror("ERROR reading from socket");
		exit(1);
	}

	if (n == 0)
	{
		printf("No more data to read\n");
		// returning false should cancel the signal handler
		return false;
	}

	return true;
}

bool GY80DataCollector::handleNewData_(Glib::IOCondition io_condition)
{
	GY80Sample sample;
	waitForSample(sample);

/*
	printf("%.2f	", buffer[GY80_NDATA]);
	forn(i, GY80_NDATA)
		printf("%.2f	", buffer[i]);
	printf("\n");
*/
	//printf("time: %.2f\n", buffer[GY80_NDATA]);
	//printf("end: '%c'\n", *((char*)(buffer+GY80_NDATA+1)));
/*
	forn(i, PKT_NDATA*sizeof(double))
		printf("%02x ", *((char*)buffer+i));
	printf("\n");
*/

	//on_new_data_.emit(buffer);
	on_new_sample_.emit(sample);

	return true;
}
