/**
 * File:   GY80DataServer.hpp
 * Author: tfischer
 * 
 * iData, an interactive data plotting software tool, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef IDATA_GY80_DATA_SERVER_HPP_
#define IDATA_GY80_DATA_SERVER_HPP_

#include <sys/types.h> 
#include <sys/socket.h>
#include <sigc++/sigc++.h>
#include <glibmm/main.h>
#include <Eigen/Core>

#define GY80_NDATA 9
#define GY80_TIME_IDX 9

struct GY80Sample
{
	double timestamp;
	Eigen::Vector3d gyro;
	Eigen::Vector3d acc;
	Eigen::Vector3d mag;
};

class GY80DataCollector
{
	public:

		void connect(int portno);

		void waitForClient();

		bool waitForSample(GY80Sample& data);

		void initAsyncRead();

		//sigc::signal<void, const double*>& onNewData();
		sigc::signal<void, const GY80Sample&>& onNewSample();

		sigc::signal<void>& onNewClient();

		sigc::signal<void>& onClientLost();

	private:

		int sockfd_, client_sockfd_;

		sigc::connection connection_on_data_ready_;

		//sigc::signal<void, const double*> on_new_data_;
		sigc::signal<void, const GY80Sample&> on_new_sample_;
		sigc::signal<void> on_new_client_;
		sigc::signal<void> on_client_lost_;

		bool waitForData_(double* buffer);
		bool handleNewData_(Glib::IOCondition io_condition);
};

#endif // IDATA_GY80_DATA_SERVER_HPP_
