#include "PlotArea.hpp"
#include "useful_macros.hpp"

PlotArea::PlotArea(const DataProvider& data_provider)
: data_provider_(data_provider)
{
  Glib::signal_timeout().connect( sigc::mem_fun(*this, &PlotArea::onTimeout_), 1000.0/PLOT_FPS );

  #ifndef GLIBMM_DEFAULT_SIGNAL_HANDLERS_ENABLED
  //Connect the signal handler if it isn't already a virtual method override:
  signal_draw().connect(sigc::mem_fun(*this, &PlotArea::on_draw), false);
  #endif //GLIBMM_DEFAULT_SIGNAL_HANDLERS_ENABLED

	range_min_ = data_provider.getRangeMin();
	range_max_ = data_provider.getRangeMax();

	// set minimum size of the widget
	set_size_request(100, 100);
	set_hexpand();
	set_vexpand();
}

PlotArea::~PlotArea()
{}

bool PlotArea::on_draw(const Cairo::RefPtr<Cairo::Context>& cr)
{
  Gtk::Allocation allocation = get_allocation();
  const int width = allocation.get_width();
  const int height = allocation.get_height();

	//std::cout << "Draw! w: " << width << " h: " << height << std::endl;

  // scale to unit square and translate (0, 0) to be (0.5, 0.5), i.e.
  // the center of the window
  //cr->scale(width, height);
  //cr->translate(0.5, 0.5);

	// paint background
	cr->save();
  cr->set_source_rgba(0.0, 0.0, 0.0, 1.0);
  cr->paint();
  cr->restore();

	cr->save();
  cr->set_source_rgba(1.0, 0.0, 0.0, 1.0);

	// TODO only do this on size change
	// let ax+b=y go from (x_min, y_min) to (x_max, y_max)
	// then
	// a = (y_max-y_min) / (x_max-x_min)
	// b = y_max - a*x_max = y_min - a*x_min
	double a = (height-0.0) / (range_max_-range_min_);
	double b = height - a*range_max_;
	a *= -1;

	// Set time scale for x range
	// 0.005 = 2 pix for a 100HZ tick
	double sec_per_pixel = 0.005;
	double last_timestamp = -1.0;

	forallr(it, data_provider_)
	{
		const DataProvider::Sample& sample = (*it);

		if (last_timestamp<0)
			last_timestamp = sample.getTime();

		double dt = last_timestamp-sample.getTime();

		int pos_x = width - 1 - dt / sec_per_pixel;
		int pos_y = ( a*sample.getValue() + b );

		if (pos_x < 0) {
			break;
		}

		cr->rectangle(pos_x, pos_y, 1, 1);
		cr->fill();
	}

	cr->restore();

  return true;
}

bool PlotArea::onTimeout_()
{
    // force our program to redraw the entire clock.
    Glib::RefPtr<Gdk::Window> win = get_window();
    if (win)
    {
        Gdk::Rectangle r(0, 0, get_allocation().get_width(),
                get_allocation().get_height());
        win->invalidate_rect(r, false);
    }
    return true;
}

