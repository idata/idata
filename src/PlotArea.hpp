/**
 * File:   PlotArea.hpp
 * Author: tfischer
 * 
 * iData, an interactive data plotting software tool, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef IDATA_PLOT_AREA_HPP_
#define IDATA_PLOT_AREA_HPP_

#include <gtkmm.h>

#include "DataProvider.hpp"

#define PLOT_FPS 30.0

class PlotArea : public Gtk::DrawingArea
{
	public:

		PlotArea(const DataProvider& data_provider);

		virtual ~PlotArea();

	protected:

		const DataProvider& data_provider_;

		double range_min_, range_max_;

		// Override default signal handler:
		virtual bool on_draw(const Cairo::RefPtr<Cairo::Context>& cr);

		bool onTimeout_();
};

#endif // IDATA_PLOT_AREA_HPP_
