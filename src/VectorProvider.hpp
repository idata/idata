#ifndef VECTOR_PROVIDER_HPP_
#define VECTOR_PROVIDER_HPP_

class VectorProvider
{
	public:

		virtual const DataProvider& getDataX() const = 0;
		virtual const DataProvider& getDataY() const = 0;
		virtual const DataProvider& getDataZ() const = 0;
};

#endif // VECTOR_PROVIDER_HPP_
