#include <map>
#include <iostream>
#include <gtkmm.h>
#include <Eigen/Core>

#include "useful_macros.hpp"
#include "PlotArea.hpp"
#include "GY80DataServer.hpp"
//#include "GY80DataProvider.hpp"
#include "FusionProvider.hpp"

#define NPLOTS 4
#define SRVR_PORTNO 4040

Gtk::DrawingArea* plots[NPLOTS][3];
Gtk::ComboBox* plot_select[NPLOTS];
Gtk::Grid* grid = NULL;
std::map<Glib::ustring, const VectorProvider*> providers;
Gtk::Grid* grid_plot[NPLOTS];

/*
class PlotConfiguration
{
	public:

		
};

class IGraphConfiguration
{
	public:

		// plot background color
		// default = ...

		// plot line color
		// default = ...

		uint8_t rows, cols;

		// matrix of size rows x cols
		PlotConfiguration** plot_config;
};
*/

class ModelColumns : public Gtk::TreeModelColumnRecord
{
	public:

		ModelColumns() { /*add(id);*/ add(name); }

		//Gtk::TreeModelColumn<guint> id;
		Gtk::TreeModelColumn<Glib::ustring> name;
};

void on_plot_selection(int column)
{
	Gtk::TreeModel::iterator it = plot_select[column]->get_active();
	ModelColumns columns;

	std::cout << "column: " << /*column << " id: '" << (*it)[columns.id] <<*/ "' name: '" << (*it)[columns.name] << "'" << std::endl;

	const Glib::ustring& plot_name = (*it)[columns.name];
	const VectorProvider* vector = providers[plot_name];
	
	PlotArea* p = new PlotArea(vector->getDataX());
	grid_plot[column]->attach(*p, 0, 1, 1, 1);
	p->show();

	p = new PlotArea(vector->getDataY());
	grid_plot[column]->attach(*p, 0, 2, 1, 1);
	p->show();

	p = new PlotArea(vector->getDataZ());
	grid_plot[column]->attach(*p, 0, 3, 1, 1);
	p->show();
}

void loadOption(Glib::RefPtr<Gtk::ListStore> option_list, const std::string& new_option)
{
	Gtk::TreeModel::iterator it = option_list->append();
	ModelColumns columns;
	//(*it)[columns.id] = 15;
	(*it)[columns.name] = new_option;
}

int main(int argc, char** argv)
{
  Gtk::Main kit(argc, argv);

  Glib::RefPtr<Gtk::Builder> builder = Gtk::Builder::create_from_file("../ui.glade");

  Gtk::Window* window = NULL;
  builder->get_widget("window", window);

	builder->get_widget("grid_plot_1", grid_plot[0]);
	builder->get_widget("grid_plot_2", grid_plot[1]);
	builder->get_widget("grid_plot_3", grid_plot[2]);
	builder->get_widget("grid_plot_4", grid_plot[3]);

	builder->get_widget("select_plot_1", plot_select[0]);
	plot_select[0]->signal_changed().connect( sigc::bind<int>( sigc::ptr_fun(&on_plot_selection), 0) );

	builder->get_widget("select_plot_2", plot_select[1]);
	plot_select[1]->signal_changed().connect( sigc::bind<int>( sigc::ptr_fun(&on_plot_selection), 1) );

	builder->get_widget("select_plot_3", plot_select[2]);
	plot_select[2]->signal_changed().connect( sigc::bind<int>( sigc::ptr_fun(&on_plot_selection), 2) );

	builder->get_widget("select_plot_4", plot_select[3]);
	plot_select[3]->signal_changed().connect( sigc::bind<int>( sigc::ptr_fun(&on_plot_selection), 3) );

	//Gtk::Button button;
	//button.signal_clicked().connect( sigc::ptr_fun(&on_button_clicked) );
	//button.signal_clicked().connect( sigc::mem_fun(some_object_ptr, &SomeClass::on_button_clicked) );
	//button.signal_clicked().connect( sigc::bind<Glib::ustring>( func, "button 1") );

	GY80DataCollector collector;
	collector.connect(SRVR_PORTNO);
	collector.waitForClient();

	GY80Sample sample_ini;
	collector.waitForSample(sample_ini);

	//CrudeVectorProvider gyro_data(collector, 0, -360, 360);
	//CrudeVectorProvider acc_data(collector, 3, -1, 1);
	//CrudeVectorProvider mag_data(collector, 6, -500, 500);
	//GravityProvider grav_data(collector, acc_ini);

	GY80Fuser fuser(sample_ini);
	collector.onNewSample().connect( sigc::mem_fun(fuser, &GY80Fuser::handleUpdate) );

	providers["gyroscope"] = &fuser.getGyro();
	providers["accelerometer"] = &fuser.getAcc();
	providers["magnetometer"] = &fuser.getMag();
	providers["gravity"] = &fuser.getGravity();
	providers["north"] = &fuser.getNorth();
	providers["linear acceleration"] = &fuser.getLinearAcceleration();

	Glib::RefPtr<Gtk::ListStore> plot_option_list = Glib::RefPtr<Gtk::ListStore>::cast_static( builder->get_object("plot_list") );
	forall(it, providers)
		loadOption(plot_option_list, it->first);

	collector.initAsyncRead();
  Gtk::Main::run(*window);

  return 0;
}
