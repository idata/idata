/**
 * File:   useful_macros.hpp
 * Author: tfischer
 * 
 * iData, an interactive data plotting software tool, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef IDATA_USEFUL_MACROS_H_
#define IDATA_USEFUL_MACROS_H_

/** useful macros */

#define forn(i,n) for(size_t i=0;i<(n);i++)
#define fornr(i,n) for(size_t i=(n)-1;0<=i;i--)
#define forsn(i,s,n) for(int i=(s);i<(n);i++)
#define forsnr(i,s,n) for(int i=(n)-1;(s)<=i;i--)
#define forall(it,X) for(auto it=(X).begin();it!=(X).end();it++)
#define forallr(it,X) for(auto it=(X).rbegin();it!=(X).rend();it++)

#endif /* IDATA_USEFUL_MACROS_H_ */
